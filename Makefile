CC=gcc
CFLAGS= -Wall -Wextra -Werror -lSDL -lSDL_image -g -std=c99

SRC_DIR = ./src/

SRC= small_sketchizer.c $(wildcard $(SRC_DIR)*.c)
OBJ=${SRC:*.c=*.o}

all : small_sketchizer 

small_sketchizer : ${OBJ}

clean :
	${RM} *.o *.d small_sketchizer test
