# small-sketchizer

A small utility to digitize your sketches rapidly using the SDL library.

# Requirements

```
sdl
sdl_image
```

You can install them using your distribution's package manager

# Compilation

Just type `make` to compile it. The binary `./small_sketchizer` is created

# How to use

Type `./small_sketchizer -h` for a list of options

A classic usage of the tool would look like this : `./small_sketchizer input_filename output_filename`