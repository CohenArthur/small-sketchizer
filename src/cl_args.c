#include "cl_args.h"

 void print_help() {
 	printf("DESCRIPTION : Super quickly digitize your sketches\n\n");
	printf("HOW TO USE : ./small_sketchizer OR ./small_sketchizer input_filename (output_filename) [OPTION] [OPTION]...\n\n");
	printf("OPTIONS :\n\t-h, --help : Displays this help\n\
\t-d, --display : Displays the result of the digitization\n\
\t-t, --threshold [INT] : Threshold for the lines of the sketch, preferably between 0 and 255. Base value is 150\n\
\t-p, --process : Displays the sketch being digitized\n");
}

struct args *get_args(int argv, char **argc) {

	struct args *args = xmalloc(sizeof(struct args));
	args->display = 0;
	args->process = 0;
	args->threshold = 127;
	args->input_filename = NULL;
	args->output_filename = NULL;

	if (argv < 2) {
		print_help();
		errx(1, "Incorrect number of arguments");
	}

	args->input_filename = calloc(1, BUFFER_SIZE);
	strcpy(args->input_filename, *(argc + 1));

	if (argv > 2) {
		for (int arg_index = 2; arg_index < argv; ++arg_index) {
			if (!strcmp(*(argc + arg_index), "-h") || !strcmp(*(argc + arg_index), "--help")) print_help();
			else if (!strcmp(*(argc + arg_index), "-d") || !strcmp(*(argc + arg_index), "--display")) args->display = 1;
			else if (!strcmp(*(argc + arg_index), "-p") || !strcmp(*(argc + arg_index), "--process")) args->process = 1;
			else if (!strcmp(*(argc + arg_index), "-t") || !strcmp(*(argc + arg_index), "--threshold")) {
				if (!*(argc + arg_index + 1)) {
					print_help();
					errx(1, "Invalid usage of argument `-t`");
				}
				args->threshold = atoi(*(argc + arg_index + 1));
				++arg_index;
			} else {
				args->output_filename = calloc(1, BUFFER_SIZE);
				strcpy(args->output_filename, *(argc + arg_index));
			}
		}
	}

	return args;
}
