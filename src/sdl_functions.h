#include <err.h>

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "xalloc.h"

#define SS_DISPLAY 1
#define SS_PROCESS 2

//FIXME : Documentation

/**
 * Waits for a key to be pressed then released
 */
void wait_for_keypressed();

/**
 * Displays a surface on the screen
 */
SDL_Surface *display_image(SDL_Surface *img);

/**
 * Returns the pixel located at width * height
 */
Uint32 get_pixel(SDL_Surface *surface, unsigned width, unsigned height);

/**
 * Sets a certain pixel at width * height 
 */
void set_pixel(SDL_Surface *surface, unsigned width, unsigned height, Uint32 pixel);

/**
 * Binarizes the surface passed as parameter 
 */
void binarize(SDL_Surface *input, int flags, int threshold);

/**
 * Cleans the image once it has been binarized
 */
void surface_clean(SDL_Surface *input, int flags);
