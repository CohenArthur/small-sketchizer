#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "xalloc.h"

#define BUFFER_SIZE 4096

struct args {
	int display;
	int process;
	int threshold;

	char *input_filename;
	char *output_filename;
};

/**
 * Prints help for the program in stdout.
 */
void print_help();

/**
 * Function returning the arguments passed 
 * The caller needs to free the arguments structure.
 * @return struct args arguments 
 */
struct args *get_args(int argv, char **argc);
