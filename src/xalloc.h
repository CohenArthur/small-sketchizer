#include <stdlib.h>
#include <err.h>

/*
 * Wrapper pour gerer les erreurs de xmalloc
 */
void *xmalloc(size_t size);

/*
 * Wrapper pour gerer les erreurs de xcalloc
 */
void *xcalloc(size_t qty, size_t size);
