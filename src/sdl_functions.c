#include "sdl_functions.h"

void wait_for_keypressed(){
	SDL_Event event;

	do{
		SDL_PollEvent(&event);
	} while (event.type != SDL_KEYDOWN);

	do{
		SDL_PollEvent(&event);
	} while (event.type != SDL_KEYUP);
}

SDL_Surface *display_image(SDL_Surface *img){
	SDL_Surface *screen;

	screen = SDL_SetVideoMode(img->w, img->h, 0, SDL_SWSURFACE|SDL_ANYFORMAT);

	if (screen == NULL)
		errx(1, "Couldn't set %dx%d video mode: %s\n",
				img->w, img->h, SDL_GetError());

	if(SDL_BlitSurface(img, NULL, screen, NULL) < 0)
		warnx("BlitSurface error: %s\n", SDL_GetError());

	SDL_UpdateRect(screen, 0, 0, img->w, img->h);
	return screen;
}

static inline
Uint8* pixel_ref(SDL_Surface *surface, unsigned x, unsigned y) {
	int bpp = surface->format->BytesPerPixel;
	return (Uint8*)surface->pixels + y * surface->pitch + x * bpp;
}

Uint32 get_pixel(SDL_Surface *surface, unsigned width, unsigned height) {
	Uint8 *p = pixel_ref(surface, width, height);

	switch(surface->format->BytesPerPixel){
		case 1:
			return *p;
		case 2:
			return *(Uint16 *)p;
		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
		case 4:
			return *(Uint32 *)p;
	}
		return 0;
}

void set_pixel(SDL_Surface *surface, unsigned height, unsigned width, Uint32 pixel) {
    Uint8 *p = pixel_ref(surface, height, width);

    switch(surface->format->BytesPerPixel){
        case 1:
            *p = pixel;
            break;
        case 2:
            *(Uint16 *)p = pixel;
            break;
        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN){
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }else{
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;
        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

void binarize(SDL_Surface *input, int flags, int threshold) {
	size_t width	 = input->w;
	size_t height	 = input->h;

	for (size_t index_w = 0; index_w < width; index_w++) {
		for (size_t index_h = 0; index_h < height; index_h++) {

			Uint32 pixel = get_pixel(input, index_w, index_h);
			Uint8 r, g, b;
			SDL_GetRGB(pixel, input->format, &r, &g, &b);

			if (r + g + b < threshold * 3) {
				r = 0;
				g = 0;
				b = 0;
				pixel = SDL_MapRGB(input->format, r, g, b);
				set_pixel(input, index_w, index_h, pixel);
			} else {
				r = 255;
				g = 255;
				b = 255;
				pixel = SDL_MapRGB(input->format, r, g, b);
				set_pixel(input, index_w, index_h, pixel);
			}
		}

	if (flags && SS_PROCESS)
		if (index_w % (width / 10) == 0)
			display_image(input);
	}
}

void surface_clean(SDL_Surface *input, int flags) {
	size_t width	 = input->w;
	size_t height	 = input->h;

	for (size_t index_w = 1; index_w < width - 1; index_w++) {
		for (size_t index_h = 1; index_h < height - 1; index_h++) {

			Uint32 pixel_top = get_pixel(input, index_w, index_h - 1);
			Uint32 pixel_bot = get_pixel(input, index_w, index_h + 1);
			Uint32 pixel_lef = get_pixel(input, index_w - 1, index_h);
			Uint32 pixel_rig = get_pixel(input, index_w + 1, index_h);
			Uint32 pixel_tol = get_pixel(input, index_w - 1, index_h - 1);
			Uint32 pixel_tor = get_pixel(input, index_w + 1, index_h - 1);
			Uint32 pixel_bol = get_pixel(input, index_w - 1, index_h + 1);
			Uint32 pixel_bor = get_pixel(input, index_w + 1, index_h + 1);

			if (pixel_top == 0xffffff &&
				pixel_bot == 0xffffff &&
				pixel_lef == 0xffffff &&
				pixel_rig == 0xffffff &&
				pixel_tol == 0xffffff &&
				pixel_tor == 0xffffff &&
				pixel_bol == 0xffffff &&
				pixel_bor == 0xffffff)
					set_pixel(input, index_w, index_h, 0xffffff);
			
		}
	if (flags && SS_PROCESS)
		if (index_w % (width / 10) == 0)
			display_image(input);

	}

}
