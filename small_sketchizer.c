#include <stdlib.h>
#include <err.h>

#include "src/cl_args.h"
#include "src/sdl_functions.h"

int main(int argv, char **argc) {
	struct args *args = get_args(argv, argc);

	if (SDL_Init(SDL_INIT_VIDEO) == -1)
		errx(1, "Couldn't initialize SDL: %s\n", SDL_GetError());

	printf("Graphical library loaded successfully...\n");
	printf("Loading input image...\n");

	SDL_Surface *img = IMG_Load(args->input_filename);
	if (!img)
		errx(3, "Couldn't load %s: %s\n", args->input_filename, IMG_GetError());

	if (args->display) {
		display_image(img);
		wait_for_keypressed();
	}

	int flags = 0;

	if (args->process)
		flags |= SS_PROCESS;

	printf("Binarizing input image...\n");

	binarize(img, flags, args->threshold);

	printf("Cleaning input image...\n");

	surface_clean(img, 0);

	if (args->display) {
		display_image(img);
		wait_for_keypressed();
	}	

	printf("Writing output image...\n");

	if (args->output_filename)
		SDL_SaveBMP(img, args->output_filename);
	else
		SDL_SaveBMP(img, args->input_filename);

	SDL_FreeSurface(img);

	free(args->input_filename);
	free(args->output_filename);
	free(args);

	return 0;
}
